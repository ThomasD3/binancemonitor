open System
open System.Collections.Concurrent
open System.Threading
open Binance.Net
open Binance.Net.Objects.Spot.MarketStream



[<EntryPoint>]
let main _ =

    // binance clients
    let restClient   = new BinanceClient()
    let socketClient = new BinanceSocketClient()

    // get the list of instruments
    let result = restClient.FuturesUsdt.System.GetExchangeInfoAsync() |> Async.AwaitTask |> Async.RunSynchronously
    if not result.Success then failwith $"couldn't get exchange info: {result.Error.Message}"
    let instruments = result.Data.Symbols |> Seq.map (fun x -> x.Name) |> Seq.toList

    // dictionary to store the last aggregated trade id and last trade id
    let lastTradeIndex = ConcurrentDictionary<string, int64 * int64>()
    do
        instruments |> List.iter (fun i -> lastTradeIndex.[i] <- (0L, 0L))

    // store the last lag values in a queue
    let queueDepth = 1000
    let lagValues = ConcurrentDictionary<string, ConcurrentQueue<double>>()
    do
        instruments |> List.iter (fun i -> lagValues.[i] <- ConcurrentQueue<double>(Array.zeroCreate queueDepth))


    // process logs in another thread to not slow down the trades stream
    let logInbox = MailboxProcessor<string>.Start(fun inbox ->
            let rec loop () = async {
                let! log = inbox.Receive()
                printfn $"{DateTime.UtcNow} {log}"
                return! loop ()
            }
            loop ()
        )


    // trade processing
    let tradesInbox = MailboxProcessor<BinanceStreamAggregatedTrade>.Start(fun inbox ->
        let rec loop () = async {
            let! trade = inbox.Receive()

            if trade = null then
                printfn "warning, received null trade"
            else

                // check the stream integrity
                let lastKnownAggId, lastKnownTradeId = lastTradeIndex.[trade.Symbol]
                if lastKnownTradeId <> 0L then
                    if trade.AggregateTradeId <> lastKnownAggId + 1L then
                        logInbox.Post $"%-10s{trade.Symbol} - ⚠️ missing aggregate trade id, went from %10i{lastKnownAggId} to %10i{trade.AggregateTradeId}, trade time: {trade.TradeTime}.{trade.TradeTime.Millisecond}"

                    elif trade.FirstTradeId <> lastKnownTradeId + 1L then
                        let fromString = $"{lastKnownAggId}:{lastKnownTradeId}"
                        let toString = $"{trade.AggregateTradeId}:{trade.FirstTradeId}"
                        logInbox.Post $"%-10s{trade.Symbol} - ⚠️ missing trades, went from %-20s{fromString} to %-20s{toString}, trade time: {trade.TradeTime}.{trade.TradeTime.Millisecond}"

                // add the lag
                lagValues.[trade.Symbol].Enqueue((DateTime.UtcNow - trade.EventTime).TotalMilliseconds)
                lagValues.[trade.Symbol].TryDequeue() |> ignore

                // update the records
                lastTradeIndex.[trade.Symbol] <- (trade.AggregateTradeId, trade.LastTradeId)


            return! loop ()
        }

        loop ()
    )

    // set a timer to monitor lags
    let interval = TimeSpan.FromSeconds(10.)
    let warningThresholdMS = TimeSpan.FromSeconds(1.).TotalMilliseconds
    let clock = new Timers.Timer(AutoReset = false, Interval = interval.TotalMilliseconds)

    clock.Elapsed.Add (fun _ ->
            let startTime = DateTime.UtcNow
            try
                let laggedCoins =
                    lagValues
                    |> Seq.map (fun kvp ->
                            match kvp.Value.ToArray() |> Array.average with
                            | lag when lag >= warningThresholdMS -> Some (kvp.Key, lag)
                            | _ -> None
                        )
                    |> Seq.choose id

                laggedCoins
                |> Seq.iter (fun (coin, lag) -> logInbox.Post $"%-10s{coin} - ⚠️ average lag is %.2f{lag / 1000.}s")

                if not (laggedCoins |> Seq.isEmpty) then
                    // reset the connection
                    ()

            finally
                let elapsed = DateTime.UtcNow - startTime
                clock.Interval <-
                    if elapsed < interval then
                        (interval - elapsed).TotalMilliseconds
                    else
                        1.
                clock.Start()
        )

    clock.Start()



    // subscribe to trades for all the instruments
    let result = socketClient.FuturesUsdt.SubscribeToAggregatedTradeUpdatesAsync(instruments, (fun x -> tradesInbox.Post x.Data)) |> Async.AwaitTask |> Async.RunSynchronously
    match result.Success with
    | true ->
        result.Data.add_ConnectionLost     (fun _ -> logInbox.Post "⚠️ trades socket connection lost")
        result.Data.add_ConnectionRestored (fun _ -> logInbox.Post "⚠️ trades socket connection restored")
        printfn $"subscribed to {instruments.Length} instruments"

    | false ->
        failwith $"❌ couldn't subscribe to trades: {result.Error.Message}"


    // let's wait here for a little while
    Thread.Sleep(Timeout.Infinite)
    0
