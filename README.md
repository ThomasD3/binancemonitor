# BinanceMonitor

I wrote this tool to monitor the stability of the futures trades stream from Binance.
It doesn't handle spot, but this can be added very easily.

The tool is quite simple and will do a few things:

- Connect to Binance Futures and retrieve the list of all the traded coins.
- Subscribe to the aggregated trades stream.
- Notify of websocket disconnection / reconnection events.
- Make sure that all trades received are sequential. If something is missing a warning is printed.
- Monitor the average lag and issue a warning every 10 seconds if the lag is over 1 second.


I noticed that once there is a lag established, it doesn't go away quickly:
For example if there is an issue with the connection, trades will start to be received later with a delay.
However that delay doesn't seem to go away on its own and it looks like the only fix is to reset the connection.
I wrote this tool specifically to look at this problem.

